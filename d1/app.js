const express = require("express") //gets the contents of express from node modules
const mongoose = require("mongoose") //gets the contents of mongoose from node modules

const app = express() //creates a server using express
const port = 3000 //sets the port for the server

//connecting to the database
mongoose.connect("mongodb+srv://database-admin:test_1234@batch63.vaqkx.mongodb.net/to_do_list?retryWrites=true&w=majority")

//Creating a new connection
let db = mongoose.connection;
//Check if connection contains error
db.on("error", console.error.bind(console, "connection error"))
//If connection succeeds
db.once("open", () => {console.log("we are connected to the server")})

//Creation of Schema
const taskSchema = new mongoose.Schema({
	name: String,
	isDone: {
		type: Boolean,
		default: false
	}
})

//Creation of Model
//Task is a model that uses the blueprint specified in taskSchema
const Task = mongoose.model("Task", taskSchema)

//Setup for allowing the server to handle request body data
app.use(express.json()) //allows the server to get json data from the user
app.use(express.urlencoded({extended:true})) // this allows the server to handle form data



app.get("/", (req, res) => {
	res.send("Hello World")
}) 
//Creates a route for our backend server
//a route is an endpoint for a server
//ex. facebook.com/danyelpo
//req stands for request
//res stand for response

/*
What this does is this:
When we go to localhost:3000/, the server would send a response with the text "Hello World"
*/

//Get all the tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, tasks) => { //Calls the mongoose model and uses its find method with no parameters (eg all tasks), and places the result in the anonymouse function with err and tasks.
	//err only gets populated if there is a problem in the query, like sudden disconnections etc
	//tasks get populated based on the result of the query
		if(err){
			return console.log("Error") //The console here is NOT the console in our browser, but rather the terminal window where the server is hosted
		} else {
			return res.status(200).json({ //status 200 means that everything is okay in terms of process
				data: tasks //.json means that the response would return the output in JSON format
			})
		}
	})
} )

//get a single task
app.get("/tasks/:id", (req, res) => {
	Task.findById(req.params.id, (err, task) => {
		if(err){
			return console.log(err)
		} else {
			return res.status(200).json({
				data: task
			})
		}
	})
})


app.get("/tasks/:id", (req, res) => {
	Task.findById(req.params.id, (err, task) => { //The findById method uses the id of the entry to search.
		//It is similar to find({_id: <id>})
		//req.params.id -> gets the parameters from the request. specify the value in the URL
		//ex. /tasks/4 -> 4 then corresponds to the id as given in the /tasks/:id route
		//task is used instead of tasks because it pulls only a single task
		if(err){
			return console.log(err)
		} else {
			return res.status(200).json({
				data: task
			})
		}
	})
})


//Create a new task
app.post("/tasks", (req, res) => {
	//creates a new Task object with a field name and value = request body's name field.
	Task.findOne({name: req.body.name}, (err, task) => {
		if(task != null && task.name == req.body.name){
		//the if condition checks if the result is not empty and the name matches the request body.
			return res.send("duplicates found")
		} else {
			let newTask = new Task ({
			name: req.body.name
			})

			//saves the new task to the database
			newTask.save((saveErr, newTask) => {
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(201).send("New Task Created")
				}
			})
		}
	})
})


//Update a task
app.put("/tasks/:id", (req, res) => {
	Task.findById(req.params.id, (err, task) => {
		if (task.isDone){
			task.isDone = false
		} else {
			task.isDone = true
		} 
		task.save((saveErr, updatedTask) => {
			if (saveErr){
				return console.err(saveErr)
			} else {
				return res.status(201).send("Task Updated")
			}
		})
	})
})


app.put("/tasks/:id", (req, res) => {
	Task.findById(req.params.id, (err, task) => {
		//assign the name from the request body
		task.name = req.body.name
		task.save((saveErr, updatedTask) => {
			if (saveErr){
				return console.err(saveErr)
			} else {
				return res.status(201).send("Task updated")
			}
		})
	})
})




//Delete a task
app.delete("/tasks/:id", (req, res) => {
	//find the task corresponding the id
	Task.findById(req.params.id, (err, task) => {
		task.remove();
		return res.status(201).send("Task deleted")
	})
})


app.listen(port, () => {console.log(`Server started at port ${port}`)} )
/*app.listen takes in 2 arguments
1 - port it would listen to
2 - what to do once its connected */